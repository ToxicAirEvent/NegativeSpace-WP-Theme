# Negative Space - A Wordpress Theme
Why did I call it "Negative Space?" I don't really know. I just wanted a somewhat subdued looking wordpress theme with some darker color tones and somewhat sparse design elements. I just threw a name on it. Don't judge.

If you'd like to see this theme in action you can see it in action at [Favorite Things](https://favoritethings.jackmceachern.com/ "Favorite Things") and [Highlights Arena](https://blog.highlightsarena.com/ "Highlights Arena").

# Features
- Works with wordpress obviously.
- Supports a custom sidebar for widget content.
- Desktop and mobile formatting.
- Custom header image support.
- Easy implementation of analytics tracking scripts.

# Installation
1. Drop the themes folder into the /wp-content/themes directory.
2. Enable the theme from the Wordpress admin panel.
3. If you want to use Google Analytics or any other analytics tracking platform paste the tracking script into the analytics.php file. 
	- You may want to add the ananlytics.php file to your gitignore listing.

You're good to go!

#### License
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

For commercial use please contact me at jackemceachern[at]gmail.com

#### Support
This theme is provided as is with no warranties or support of any kind. Feel free to open any issues, or bring any bugs or feature request to my attention. I will do my best to help where I can but it should be assumed no support will be provided.