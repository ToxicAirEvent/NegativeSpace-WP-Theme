<?php
add_action('after_setup_theme', 'register_my_menu');
function register_my_menu() {
	register_nav_menu( 'PrimaryNavigationMenu', __( 'Main Navigation Menu', 'theme-slug' ) );
}

add_action('widgets_init', 'ctbl_sidebar_init');
	
function ctbl_sidebar_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Widgets', 'NegativeSpace-PhotoBlog' ),
		'id'            => 'ctbl-sidebar',
		'description'   => __( 'What do you want displayed in your sites sidebar?'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}

add_theme_support('post-thumbnails');

$header_defaults = array(
	'default-image'          => get_template_directory_uri() . '/images/header.png',
	'width'                  => 125,
	'height'                 => 125,
	'flex-height'            => false,
	'flex-width'             => false,
	'uploads'                => true,
	'random-default'         => false,
	'header-text'            => true,
	'default-text-color'     => '',
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
);
add_theme_support('custom-header', $header_defaults);


/* Add your analytics tracking code into the analytics.php file in the root directory of this theme. */
add_action('wp_footer', 'get_google_analytics');
function get_google_analytics() {
	$path_to_theme = get_template_directory();
	$analytics_path = $path_to_theme . "/analytics.php";
	include($analytics_path);
}

function custom_meta_data($wp_customize){
	$wp_customize->add_section('homepage_meta_options_section',
		[
			'title' => 'Site Meta Description',
			'priority' => 40,
			'description' => 'Enter a site meta description which will appear on the homepage or if one is not available.'
		]
	);
	
	$wp_customize->add_setting('homepage_meta_options_settings', 
		[
			'default'        => 'A wordpress blog.',
			'capability'     => 'edit_theme_options',
			'type'           => 'theme_mod',
		]
	);
 
    $wp_customize->add_control('homepage_meta_options_settings', array(
        'label'      => __('Site Meta Description', 'themename'),
		'description' => 'The meta description which will be used for your site when one the homepage or no other one is available.',
        'section'    => 'homepage_meta_options_section',
        'type'   => 'textarea',
    ));
}

add_action('customize_register', 'custom_meta_data');

function load_custom_scripts()
{
wp_register_script('customscripts', get_template_directory_uri() . '/js/image-link-styler.js', array('jquery'), '3.4.1', true);
wp_enqueue_script('customscripts');
}

add_action('wp_enqueue_scripts', 'load_custom_scripts');
 ?>