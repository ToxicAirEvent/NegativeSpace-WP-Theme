<div id="sidebar">
		<?php if ( is_active_sidebar( 'ctbl-sidebar' ) ) : ?>
			<div id="widget-area" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'ctbl-sidebar' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>
</ul>
</div>