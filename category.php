<?php get_header(); ?>

<!--The content wrapper is closed in the footer.php file of the theme.-->
<div id="wrapper">

<div id="main">
	<div id="content">

	<h1 class="categoryHeader">Browsing Post Categorized As <span class="categoryTitle"><?php echo single_cat_title(); ?></span></h1>

		<?php if (have_posts()) : while (have_posts()) : the_post(); 
			$author = get_the_author('');
		?>

	<div class="post">

		<h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
		<h4>By: <i><?php echo $author; ?></i> on <?php the_time('F jS, Y') ?></h4>
		
		<?php
			if ( has_post_thumbnail() ) {
			?>
			
			<img class="SelectedImage" src="<?php the_post_thumbnail_url() ?>" />
			
			<div>
				<?php the_excerpt(); //fetches the post except to see more images after the post thumbnail is fetched. ?>
				<p><a href="<?php echo get_permalink(); ?>">///VIEW THE REST///</a></p>
			</div>
				
			<?php }else{ ?>
		
		<p><?php the_content(__('(//VIEW THE REST//)'));} //This } brace ends IF statement of Thumbnail image?></p>
	</div>

	<hr> <?php endwhile; else: ?>

	<p><?php _e('COULD NOT FIND THAT.'); ?></p><?php endif; ?>
	
	<div class="nav-previous alignleft"> <?php previous_posts_link('&lsaquo; Newest Post'); ?></div>
	<div class="nav-next alignright"><?php next_posts_link('Older Post &rsaquo;'); ?></div>
	</div>

<?php get_sidebar(); ?>
</div>

<div id="delimiter"></div>

<?php get_footer(); ?>