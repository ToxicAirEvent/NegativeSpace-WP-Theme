<?php get_header(); ?>
<div id="wrapper">
<div id="main">
<div id="content">

	<?php if (have_posts()) : while (have_posts()) : the_post(); 
		$author = get_the_author('');
	?>
<div class="post">
	<h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
	<p><?php the_content(__('(//THE REST//)')); ?></p>
	<h4>Last updated by: <b><?php echo $author; ?></b> <i>on</i> <?php the_time('F jS, Y') ?></h4>
</div>

<hr> <?php endwhile; else: ?>

<p><?php _e('NOTHING HERE...'); ?></p><?php endif; ?>
</div>

<?php get_sidebar(); ?>
</div>

<div id="delimiter">
</div>

<?php get_footer(); ?>