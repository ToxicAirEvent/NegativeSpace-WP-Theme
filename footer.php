<div id="footer">
<?php $blog_title = get_bloginfo('name'); ?>
<p>&copy; <span class="year">2019</span> | By: <a href="<?php echo get_home_url(); ?>"><?php echo trim($blog_title); ?></a></p>
</div>
</div>
<script>
var theDate = new Date();
var theYear = theDate.getFullYear();

document.getElementsByClassName("year")[0].innerHTML = theYear;
</script>

<?php wp_footer(); ?>

</body>
</html>