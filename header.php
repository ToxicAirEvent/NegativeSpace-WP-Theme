<!DOCTYPE html>
<html>
<head>
<?php

	//This is all code to conditionally set the page title based on the users location within the site.
	$final_page_title;
	$blog_title = get_bloginfo('name');
	
	if(is_home()){
		$final_page_title = $blog_title;
	}elseif(is_search()){
		$final_page_title = "Searched: " . esc_html(get_search_query(false));
	}else{
		$final_page_title = wp_title("",false) . " | " . $blog_title;
	}
	
	//Just trim out any troublesome spaces.
	$final_page_title = trim($final_page_title);

	//Set the metadescription for a each page within the site.
	$final_meta_description;
	if(is_home()){
		$final_meta_description = get_theme_mod('homepage_meta_options_settings');
	}else{
		$final_meta_description = get_the_excerpt();
	}
 ?>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $final_meta_description; ?>">

<title><?= $final_page_title ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
<?php wp_head(); ?>
</head>

<body>
<div id="header">
	<img class="headerImage" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />

	<div class="site-title retroshadow"><?php echo $blog_title; ?></div>
	<?php wp_nav_menu( array( 'theme_location' => 'PrimaryNavigationMenu', 'container_class' => 'NavMenu') ); ?>
</div>